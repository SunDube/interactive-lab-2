﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketMovement : MonoBehaviour
{
    //Animation curve for the rocket speed lerp
    public AnimationCurve rocketSpeedShift;

    //The rocket's speed
    public float rocketSpeed;

    //Keeps time for the animation curve
    private float animationTime;

    //The rockets current destination
    private Vector3 destination;

    //The flag object the rocket places
    public GameObject colonisationFlag;

    //The rotation direction the rocket must face
    private Quaternion rocketRotate;

    //The calculated angle of rotation
    private float rotAngle;

    //The speed of rotation
    public float rotationSpeed;

    //How much fuel the rocket has
    public int fuelReserve;

    //The number of planets the rocket has touched
    public int planetsTaken;

    //The animator on the rocket sprites
    public Animator rocketStates;


    // Start is called before the first frame update
    void Start()
    {
        //Set the destination to current position
        destination = transform.position;
        //Sets the fuel to the maximum
        fuelReserve = 5;
    }

    // Update is called once per frame
    void Update()
    {
        //If the rocket has not reached its destination
        if (destination != transform.position && fuelReserve>-1)
        {
            //Set the time in the animation curve
            animationTime += Time.deltaTime;
            //Lerp The Rocket
            transform.position = Vector3.Lerp(transform.position, destination, rocketSpeed*rocketSpeedShift.Evaluate(animationTime));
            //Rotate the rocket
            CalculateRotation();
            //Sets animation bool as true to change the clip being played
            rocketStates.SetBool("Landed", false);

        }
        else
        {
            //Resets the animation curve
            animationTime = 0;
            //Change the animation clip being played
            rocketStates.SetBool("Landed", true);
            rocketStates.SetBool("NewDest", false);

        }

        
    }

    //Puts a flag on the planet and increases to counter
    public void PlaceFlag() {
        Instantiate(colonisationFlag, destination, Quaternion.identity);
        planetsTaken++;
    }


    //Watched this tutorial before completing this section https://www.youtube.com/watch?v=mKLp-2iseDc
    public void CalculateRotation() {
        Vector2 differenceAngle = destination - transform.position;
        rotAngle = Mathf.Atan2(differenceAngle.x, differenceAngle.y) * Mathf.Rad2Deg;
        rocketRotate = Quaternion.AngleAxis(-rotAngle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rocketRotate, rotationSpeed * Time.deltaTime);
    }

    //Sets a new destination for the rocket
    public void UpdateDestination(Vector3 newDest) {
        //Co-ordinates of destination planet
        destination = newDest;
        //Changes the animation clip
        rocketStates.SetBool("NewDest", true);
        rocketStates.SetBool("Fueling", false);
        //Deducts from the fuel reserve
        fuelReserve--;

    }

    public void Refuel() {
        //Sets fuel back to maximum
        fuelReserve = 5;
        //Changes animation clip
        rocketStates.SetBool("Fueling", true);
        rocketStates.SetBool("Landed", false);
    }
}
