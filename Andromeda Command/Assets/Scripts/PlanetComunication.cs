﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetComunication : MonoBehaviour
{
    //The location of the planet
    public Vector3 planetCoords;

    //the player rocket
    private GameObject rocket;
   
    //If the planet is dicovered/undicovered
    private bool conquored = false;

    //If the planet is a fueling station
    public bool FuelRest = false;

    void Start()
    {
        //Load the player game object
        rocket = GameObject.FindGameObjectWithTag("Player");
        //Sets the location of the planet
        planetCoords = this.transform.position;

        //Makes the planet automatically discovered
        if (FuelRest == true) {
            conquored = true;
        }

    }

    void OnMouseDown()
    {
         Debug.Log("new destination set");

        //Sets the planet as the rockets destination
        rocket.GetComponent<RocketMovement>().UpdateDestination(planetCoords);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Checks if the rocket is on the planet
        if (collision.gameObject.tag == "Player") {

            //Places a flag on the planet the first time it is found
            if (conquored == false && FuelRest == false)
            {
                Debug.Log("Planet was conquored");
                rocket.SendMessage("PlaceFlag");
                conquored = true;
            }
            else
            {
                Debug.Log("You've already been here");
            }

            //Refules the rocket if the planet is a fueling station
            if (FuelRest == true) {
                rocket.GetComponent<RocketMovement>().Refuel();
            }

        }
    }
}
