﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceManager : MonoBehaviour
{
    //This script updates the UI elements with the rocket's stats


    //Number of planets that have been discovered
    private int plantesConquered;
    //The current amount of fuel in the rocket
    private int currentFuel;
    //The total number of planets available to discover
    public int numOfPlanets;
    //Variable to change the fuel display bar
    private Vector3 currentFuelLevel;
    //The planet discovery count text
    Text planetProgress;
    //The fuel display bar
    RectTransform fuelGuage;
  
    // Start is called before the first frame update
    void Start()
    {
        UpdateStats();
        planetProgress = GameObject.FindGameObjectWithTag("PlanetCounter").GetComponent<Text>();
        fuelGuage = GameObject.FindGameObjectWithTag("FuelLevel").GetComponent<RectTransform>();
       
    }

    // Update is called once per frame
    void Update()
    {

        UpdateStats();
        //Update the discovery progress text
        planetProgress.text = "Planets conquered: \t" + plantesConquered + "/" + numOfPlanets;

        CalculateCurrentFuel();
        //Update the fuel bar by shanging the scale of the transform
        fuelGuage.localScale = Vector3.Lerp(fuelGuage.localScale, currentFuelLevel, 0.1f);

        //Waits a bit to change the scene as the quick scene change was jarring
        Invoke("CheckSceneChange",6);
    }

    //Fetches the statistics from the rocket
    void UpdateStats() {
        plantesConquered = this.gameObject.GetComponent<RocketMovement>().planetsTaken;
        currentFuel = this.gameObject.GetComponent<RocketMovement>().fuelReserve;
    }

    //Calculates which values to change the display bar to 
    void CalculateCurrentFuel() {
        if (currentFuel <= 5) {
            float fuelPercentage = currentFuel * 0.2f;
            currentFuelLevel = new Vector3(fuelPercentage,fuelGuage.localScale.y);
        }
    }

    //Checks if the win/loss conditions have been met
    void CheckSceneChange() {
        //If player finds all the planets (Win)
        if (plantesConquered == numOfPlanets) {
            this.gameObject.GetComponent<SceneSwitch>().LoadVictoryScene();
        }

        //If the player runs out of fuel (Lose)
        if (currentFuel < 0)
        {
            this.gameObject.GetComponent<SceneSwitch>().LoadFailScene();
        }
    }
}
