﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour
{
    //The player character
    private GameObject rocketship;

    //The constant difference between the player and the camera
    private Vector3 offset;

    
    void Start()
    {
        //Load the player character
        rocketship = GameObject.FindGameObjectWithTag("Player");
        //Set the offset
        offset = transform.position - rocketship.transform.position;
    }

    

    void LateUpdate()
    {
        //moves the camera to the player position
        transform.position = rocketship.transform.position + offset;
    }
}
