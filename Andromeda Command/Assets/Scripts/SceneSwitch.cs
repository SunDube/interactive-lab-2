﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour
{
    //This class will switch between the different scenes

    //Switches to gameplay scene
    public void LoadMainScene()
    {
        SceneManager.LoadScene(1);
    }

    //Switches to victory screen
    public void LoadVictoryScene()
    {
        SceneManager.LoadScene(2);
    }

    //Switches to fail screen
    public void LoadFailScene()
    {
        SceneManager.LoadScene(3);
    }
}
